package gson;

import java.util.Collections;
import java.util.List;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    List<Computer> computerList;
    GSONParser gsonParser = new GSONParser("computerJSON.json");
    computerList = gsonParser.parse();
    Collections.sort(computerList);
    logger.warn(computerList);
  }
}
