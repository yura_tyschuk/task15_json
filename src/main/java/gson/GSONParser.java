package gson;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import model.Computer;

public class GSONParser {

  private String jsonFile;

  public GSONParser(String jsonFile) {
    this.jsonFile = jsonFile;
  }

  public List<Computer> parse() {
    Computer[] computer = new Computer[0];
    try {
      Gson gson = new Gson();
      computer = gson.fromJson(new FileReader(jsonFile), Computer[].class);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    return Arrays.asList(computer);
  }
}
