package jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import model.Computer;

public class JacksonParser {

  private ObjectMapper objectMapper;
  private String jsonFile;

  public JacksonParser(String jsonFile) {
    this.jsonFile = jsonFile;
    this.objectMapper = new ObjectMapper();
  }

  public List<Computer> getComputerList() {
    Computer[] computers = new Computer[0];
    try {
      computers = objectMapper.readValue(new File(jsonFile), Computer[].class);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return Arrays.asList(computers);
  }
}
