package jackson;

import java.util.ArrayList;
import java.util.List;
import model.Computer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    List<Computer> jsonList;
    String jsonFile = "computerJSON.json";
    JacksonParser jacksonParser = new JacksonParser(jsonFile);
    jsonList = jacksonParser.getComputerList();
    logger.warn(jsonList);
  }
}
