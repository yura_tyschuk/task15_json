package model;

public class Ports {

  private boolean comPort;
  private boolean usbPort;
  private boolean lptPort;

  public Ports() {
  }

  public boolean getComPort() {
    return comPort;
  }

  public void setComPort(boolean comPort) {
    this.comPort = comPort;
  }

  public boolean getUsbPort() {
    return usbPort;
  }

  public void setUsbPort(boolean usbPort) {
    this.usbPort = usbPort;
  }

  public boolean getLptPort() {
    return lptPort;
  }

  public void setLptPort(boolean lptPort) {
    this.lptPort = lptPort;
  }


  @Override
  public String toString() {
    return "Usb port: " + usbPort + " LPT port: " + lptPort + " COM port: " + comPort;
  }

}
