package model;

public class Types {

  private boolean periphery;
  private double energyConsumption;
  private boolean cooler;
  private ComponentsGroup componentsGroup;

  public Types() {
  }

  public boolean isPeriphery() {
    return periphery;
  }

  public void setPeriphery(boolean periphery) {
    this.periphery = periphery;
  }

  public double getEnergyConsumption() {
    return energyConsumption;
  }

  public void setEnergyConsumption(double energyConsumption) {
    this.energyConsumption = energyConsumption;
  }

  public boolean isCooler() {
    return cooler;
  }

  public void setCooler(boolean cooler) {
    this.cooler = cooler;
  }

  public ComponentsGroup getComponentsGroup() {
    return componentsGroup;
  }

  public void setComponentsGroup(ComponentsGroup componentsGroup) {
    this.componentsGroup = componentsGroup;
  }

  @Override
  public String toString() {
    return " periphery: " + periphery + " energy consumption: " + energyConsumption
        + " cooler: " + cooler + " components group: " + componentsGroup;
  }
}
