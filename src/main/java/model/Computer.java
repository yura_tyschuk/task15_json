package model;

public class Computer implements Comparable<Computer> {

  private int computerNumber;
  private String computerName;
  private String origin;
  private double price;
  private Types types;
  private Ports ports;
  private boolean critical;

  public Computer() {

  }

  public Computer(int computerNumber, String computerName, String origin, double price, Types types,
      Ports ports, boolean critical) {
    this.computerNumber = computerNumber;
    this.computerName = computerName;
    this.origin = origin;
    this.price = price;
    this.types = types;
    this.ports = ports;
    this.critical = critical;
  }

  public Ports getPorts() {
    return ports;
  }

  public void setPorts(Ports ports) {
    this.ports = ports;
  }

  public int getComputerNumber() {
    return computerNumber;
  }

  public void setComputerNumber(int computerNumber) {
    this.computerNumber = computerNumber;
  }

  public String getComputerName() {
    return computerName;
  }

  public void setComputerName(String computerName) {
    this.computerName = computerName;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Types getTypes() {
    return types;
  }

  public void setTypes(Types types) {
    this.types = types;
  }

  public boolean isCritical() {
    return critical;
  }

  public void setCritical(boolean critical) {
    this.critical = critical;
  }

  @Override
  public String toString() {
    return "Computer number: " + computerNumber + " computer name: " + computerName
        + " origin: " + origin + " price: " + price + " ports: " + ports
        + " type: " + types + " critical: " + critical + "\n";
  }

  @Override
  public int compareTo(Computer o) {
    return this.computerName.compareTo(o.computerName);
  }
}
