package model;

public class ComponentsGroup {

  private String processor;
  private int RAM;
  private String videoCard;
  private String motherBoard;

  public ComponentsGroup() {
  }

  public String getProcessor() {
    return processor;
  }

  public void setProcessor(String processor) {
    this.processor = processor;
  }

  public int getRAM() {
    return RAM;
  }

  public void setRAM(int RAM) {
    this.RAM = RAM;
  }

  public String getVideoCard() {
    return videoCard;
  }

  public void setVideoCard(String videoCard) {
    this.videoCard = videoCard;
  }

  public String getMotherBoard() {
    return motherBoard;
  }

  public void setMotherBoard(String motherBoard) {
    this.motherBoard = motherBoard;
  }

  @Override
  public String toString() {
    return " processor: " + processor + " RAM: " + RAM
        + " video card: " + videoCard + " mother board: "
        + motherBoard;
  }
}
