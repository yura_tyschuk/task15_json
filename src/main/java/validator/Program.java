package validator;

import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    File jsonFile = new File("computerJSON.json");
    File jsonSchema = new File("jsonSchema.json");
    JsonValidation jsonValidation = new JsonValidation(jsonFile, jsonSchema);
    logger.warn(jsonValidation.isValide());
  }
}
