package validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.IOException;

public class JsonValidation {
  private File jsonFile;
  private File jsonSchema;

  public JsonValidation(File jsonFile, File jsonSchema) {
    this.jsonFile = jsonFile;
    this.jsonSchema = jsonSchema;
  }

  public boolean isValide() {
    try {
      JsonNode jsonFile = JsonLoader.fromFile(new File("computerJSON.json"));
      JsonNode schemaFile = JsonLoader.fromFile(new File("jsonSchema.json"));

      JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
      com.github.fge.jsonschema.main.JsonValidator validator = factory.getValidator();
      ProcessingReport report = validator.validate(schemaFile, jsonFile);

      return report.isSuccess();
    } catch(IOException | ProcessingException e) {
      e.printStackTrace();
    }
    return false;
  }
}
